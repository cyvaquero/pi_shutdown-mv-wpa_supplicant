#! /bin/bash

# Raspbian ships with a service which facilitates headless wireless
# configuration by moving /boot/wpa_supplicant.conf, if it exists, from /boot
# to /etc/wpa_supplicant/.

# This service performs the reverse, moving wpa_supplicant.conf from
# /etc/wpa_supplicant back out to /boot on system shutdown.
# This allows building on the existing wpa_supplicant.conf configurations versus
# having to recreate it when switching SSIDs.

DIR="$( cd "$( /usr/bin/dirname "${BASH_SOURCE[0]}" )" && /bin/pwd )"

# Copy mv-wpa_supplicant.sh and mv-wpa_supplicant.service to their respective
# locations.
if [ ! -e /root/scripts ]; then /bin/mkdir /root/scripts; fi
/bin/cp $DIR/mv-wpa_supplicant.sh /root/scripts/mv-wpa_supplicant.sh
/bin/cp $DIR/mv-wpa_supplicant.service /etc/systemd/system/mv-wpa_supplicant.service

# Enable the mv-wpa_supplicant.service in systemd.
/bin/systemctl enable mv-wpa_supplicant