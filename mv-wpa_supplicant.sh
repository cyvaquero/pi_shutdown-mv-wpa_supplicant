#!/bin/bash

# This scipt moves the /etc/wpa_supplicant/wpa_supplicant.conf file to
# /boot/wpa_supplicant.conf where it can be editted on non-Linux machines
# when the SD card is inserted. /boot/wpa_supplicant.conf is copied back to
# /etc/wpa_supplicant/wpa_supplicant.conf on boot, this functionality is
# managed via the raspberrypi-net-mods package.

if [ -e /etc/wpa_supplicant/wpa_supplicant.conf ]; then
    /bin/mv /etc/wpa_supplicant/wpa_supplicant.conf /boot/wpa_supplicant.conf
fi